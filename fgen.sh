#!/bin/bash

# SPDX-License-Identifier: GPL-2.0
# Copyright (c) 2023 Hugo Villeneuve <hugo@hugovil.com>

# Abort on all errors
set -e

PROG_NAME=$(basename $0)

# Set default values
bg_layer_name="background"
ref_layer_prefix="ref"

# Pour conversion vidéo:
fps=8

debug=0
trames=1
dry_run=0
global=0

SED=sed

F_FMT="%04d"

# When reading a file line by line, if a command inside the loop
# also reads stdin, it can exhaust the input file.
# -nostdin: disable ffmpeg interaction on standard input:
FFMPEG_OPTS="-hide_banner -loglevel error -y -nostdin"

# -limit memory 5000mb -limit disk 5gb
IM_OPS=""

case ${OSTYPE} in
    darwin*)
        SED=gsed
        ;;
esac

COLOR_ERR="\033[0;31m"
COLOR_WARN="\033[0;33m"
COLOR_NONE="\033[0m"

function log_info() {
    echo -e "${COLOR_NONE}${*}"
}

function log_dbg() {
    if [ x"${debug}" = x"1" ]; then
        echo "${*}"
    fi
}

function log_warn() {
    echo -e "${COLOR_WARN}${*}${COLOR_NONE}"
}

function log_err() {
    echo -e "${COLOR_ERR}${*}${COLOR_NONE}"
}

# Arg1: layer
function get_size()
{
    cat ${layers} | grep -m 1 "${1}" | ${SED} "s/.*geometry:\([0-9]\+x[0-9]\+\).*/\1/g"
}

# Arg1: line
function get_pos()
{
    local token
    token=$(echo "${1}" | ${SED} "s/.*geometry:[0-9]\+x[0-9]\+\([+-][0-9]\+[+-][0-9]\+\),.*/\1/g")
    echo "${token}"
}

# Arg1: line
function get_label()
{
    local token
    token=$(echo "${1}" | ${SED} "s/.*label:\(.*\),geometry.*/\1/g")
    echo "${token}"
}

# Arg1: label
function get_frame_id()
{
    local token
    token=$(echo "${1}" | grep -e "^[0-9]\+" | ${SED} "s/^\([0-9]\+\).*/\1/g")
    echo "${token}"
}

# Arg1: label
function get_prefix_ref()
{
    local w
    local token

    for w in ${ref_layer_prefix}; do
        token=$(echo "${1}" | grep -e "^${w}" || echo "")

        if [ "${token}" != "" ]; then
            # Ignore image.
            echo "${token}"
            return 0
        fi
    done

    # Do not ignore image
    echo ""
}

# Arg1: line
function get_scene_id()
{
    local token
    token=$(echo "${1}" | ${SED} "s/.*scene:\([0-9]\+\),.*/\1/g")
    echo "${token}"
}

# Arg1: label
function get_group_id()
{
    local token
    token=$(echo "${1}" | grep -e "^groupe" | ${SED} "s/^groupe-\(.*\)/\1/g")
    echo "${token}"
}

# Arg1: group name
function get_group_ref()
{
    local token
    token=$(echo "${1}" | grep -e "^ref" | ${SED} "s/^\(ref\).*/\1/g")
    echo "${token}"
}

# Get duplicate from. Ex: "dup1-7 planXYZ,geometry..." will return 1
# Arg1: label
function get_dup_from()
{
    local token
    token=$(echo "${1}" | grep -e "^dup" | ${SED} "s/^dup\([0-9]\+\)-.*/\1/g")
    echo "${token}"
}

# Get duplicate to. Ex: "dup1-7 planXYZ,geometry..." will return 7
# Arg1: label
function get_dup_to()
{
    local token
    token=$(echo "${1}" | grep -e "^dup" | ${SED} "s/^dup[0-9]\+-\([0-9]\+\).*/\1/g")
    echo "${token}"
}

# Generate frame index name with leading zeroes:
# Arg1: frame number
function get_frame_name()
{
    local index

    index=$(printf "${F_FMT}" ${1})
    echo "${dest}/${dest}-${group}-f${index}.png"
}

# Arg1: start frame
# Arg1: end frame
function insert_empty_frames()
{
    local w
    local start
    local end
    local wfname

    start=${1}
    end=${2}

    for w in $(seq ${start} ${end}); do
        log_dbg "New frame ID: ${w} (empty)"

        wfname=$(get_frame_name ${w})

        cp ${dest}/background.png ${wfname}
    done
}

function record_max_nf()
{
    if [ ${nf} -gt ${max_nf} ]; then
        max_nf="${nf}"
    fi
}

function generate_video()
{
    local png_files

    if which ffmpeg 1> /dev/null 2>&1; then
        png_files=$(ls ${dest}/${dest}-${group}-f*.png 2> /dev/null || echo "")

        if [ x"${png_files}" != x"" ]; then
            group_list="${group_list} ${group}"

            log_dbg "generate_video start"

            local outfile

            outfile=${dest}/${dest}-${group}.mp4

            if [ ${dry_run} -eq 1 ]; then
                touch ${outfile}
            else
                ffmpeg ${FFMPEG_OPTS} -r ${fps} -pattern_type glob -i "${dest}/${dest}-${group}-f*.png" ${outfile}
            fi

            log_dbg "generate_video: end"
        fi
    fi
}

function generate_global()
{
    local g
    local w
    local files
    local group
    local fname

    log_dbg "generate_global: start"
    log_dbg "  list:   ${group_list}"
    log_dbg "  max_nf: ${max_nf}"

    for w in $(seq 1 ${max_nf}); do
        files=""

        for g in ${group_list}; do
            group=${g}
            fname=$(get_frame_name ${w})

            if [ -f "${fname}" ]; then
                files="${files} ${fname}"
            fi
        done

        if [ "${files}" != "" ]; then
            group="global"
            fname=$(get_frame_name ${w})

            if [ ${dry_run} -eq 1 ]; then
                touch ${fname}
            else
                convert -colorspace sRGB -page +0+0 ${dest}/background-white.png \
                        ${files} -background none -layers merge \
                        PNG32:${fname}
            fi
        fi
    done

    group="global"
    generate_video

    log_dbg "generate_global: end"
}

print_usage()
{
    echo "${PROG_NAME} -- Générateur de séquence d'images PNG à partir d'un fichier Photoshop (PSD/PSB)."
    echo "Usage: ${PROG_NAME} [OPTIONS...] FICHIER-PHOTOSHOP"
    echo
    echo "Options:"
    echo "  -d   affiche les informations de debug"
    echo "  -f   ne regénère pas les trames, mais uniquement la séquence vidéo"
    echo "  -g   génère les fichiers globaux"
    echo "  -n   mode de test (dry-run)"
    echo "  -r   nombre de trames par seconde (FPS)"
    echo "  -s   préfixe d'image à ignorer"
    echo "  -h   affiche ce message d'aide"
}

while getopts "dhfgnr:s:" flag ;do
    case ${flag} in
        d)
            debug="1"
            ;;
        f)
            trames=0
            ;;
        g)
            global=1
            ;;
        n)
            dry_run=1
            ;;
        r)
            fps="${OPTARG}"
            ;;
        s)
            ref_layer_prefix="${ref_layer_prefix} ${OPTARG}"
            ;;
	h)
	    print_usage
            exit 0
            ;;
        ?)
            echo "${PROG_NAME}: Option invalide: ${OPTARG}."
            echo "Essayez \`${PROG_NAME} -h' pour plus d'informations."
            exit 1
            ;;
    esac
done
shift `expr "${OPTIND}" - 1`

# `$#' now represents the number of arguments after the options.
# `$1' is the first argument, etc.
if [ $# -eq 0 ]; then
    echo "${PROG_NAME}: Nom de fichier source manquant."
    echo "Essayez \`${PROG_NAME} -h' pour plus d'informations."
    exit 1
fi

src="${*}"

if [ ! -f "${src}" ]; then
    log_err "Erreur: fichier source non trouvé"
    exit 1
fi

ext=$(echo "${src##*.}" | awk '{print tolower($0)}')

case ${ext} in
    psd|psb)
        # Ok: PSD or PSB format detected
        ;;
    *)
        log_err "Format non-supporté: ${ext}"
        exit 1
        ;;
esac

dest=$(basename -s .${ext} "${src}")
layers=${dest}/layers.txt

if [ ! -d ${dest} ]; then
    mkdir ${dest}
fi

for tool in convert composite identify; do
    if ! which ${tool} 1> /dev/null 2>&1; then
        echo "Utilitaire '${tool}' manquant (installer ImageMagick)."
        exit 1
    fi
done

if [ ${trames} -eq 1 ]; then
    # %s: scene number
    identify ${IM_OPS} -verbose -format "scene:%s,label:%l,geometry:%g,\n" "${src}" > ${layers}

    # Remove line(s) with empty label:
    ${SED} -i -e /label:,.*/d ${layers}

    # Extract global frame size:
    size=$(get_size "${bg_layer_name}")

    log_dbg "Background size: ${size}"

    if [ x"${size}" = x"" ]; then
        log_err "Error: background layer not found"
        exit 1
    fi

    # Remove background line:
    ${SED} -i -e /label:${bg_layer_name},.*/d ${layers}

    # Create transparent background frame:
    convert -size ${size} xc:none ${dest}/background.png

    # Create white background frame:
    convert -size ${size} xc:white ${dest}/background-white.png

    nf=""
    max_nf="0"
    oldnf=""
    files=""
    group="default"
    group_list=""

    while read l; do
        scene=$(get_scene_id "${l}")
        label=$(get_label "${l}")
        p=$(get_pos "${l}")

        nf=$(get_frame_id "${label}")
        ng=$(get_group_id "${label}")
        dup_from=$(get_dup_from "${label}")
        dup_to=$(get_dup_to "${label}")

        # Also indicate a new frame, but to be copied from..to:
        if [ x"${dup_from}" != x"" ]; then
            nf=${dup_from}
        fi

        log_dbg "Layer ${scene}"
        log_dbg "  nom: ${label}"
        log_dbg "  pos: ${p}"

        prefix_ref=$(get_prefix_ref "${label}")
        group_ref=$(get_group_ref "${group}")
        log_dbg "  group_ref: ${group_ref}"

        # Only change group if ng is set...
        if [ x"${ng}" != x"" ]; then
            if [ x"${group_ref}" != x"ref" ]; then
                generate_video
            fi

            group="${ng}"
            log_info "New group: ${ng}"

            nf=""
            oldnf=""
            files=""

            # Skip image from group ID...
            continue
        fi

        if [ x"${group_ref}" != x"" ]; then
            # Ignore all images in groupe-ref:
            log_dbg "  ignore (groupe-ref)"
            continue
        fi

        # Ignore images beginning with a specific reference prefix:
        if [ x"${prefix_ref}" != x"" ]; then
            # Ignore all images in prefix-ref:
            log_dbg "  ignore (prefix-ref)"
            continue
        fi

        files="${files} -page ${p} ${src}[${scene}]"

        if [ x"${nf}" != x"" ]; then
            # Only check expected frame if oldnf is set...
            if [ x"${oldnf}" != x"" ]; then
                expected_nf=$((${oldnf} + 1))

                if [ ${expected_nf} -ne ${nf} ]; then
                    log_warn "Warning: non-sequential frame sequence: ${nf}"
                    log_warn "  previous: ${oldnf}"
                    log_warn "  expected: ${expected_nf}"
                    insert_empty_frames ${expected_nf} $((${nf} -1))
                fi
            fi

            log_dbg "New frame ID: ${nf}"
            record_max_nf

            fname=$(get_frame_name ${nf})

            if [ ${dry_run} -eq 1 ]; then
                touch ${fname}
            else
                convert -colorspace sRGB -page +0+0 ${dest}/background.png \
                        ${files} -background none -layers merge \
                        PNG32:${fname}
            fi

            if [ x"${dup_from}" != x"" ]; then
                log_dbg "  dup from: ${dup_from}"
                log_dbg "  dup to:   ${dup_to}"

                dup_start=$((${dup_from} + 1))
                for w in $(seq ${dup_start} ${dup_to}); do
                    log_dbg "New frame ID: ${w} (duplicate)"

                    wfname=$(get_frame_name ${w})

                    # Use symlink???
                    cp ${fname} ${wfname}

                    nf=${w}
                done
            fi

            files=""
            oldnf="${nf}"
        fi
    done <${layers}
fi

# Special case for last group...
generate_video

if [ x"${global}" = x"1" ]; then
    generate_global
fi
