#!/bin/sh

git fetch --all
git reset --hard origin/master

if [ ! -d /usr/local/bin ]; then
    sudo install -d /usr/local/bin/
fi

sudo install -m0755 fgen.sh /usr/local/bin/
